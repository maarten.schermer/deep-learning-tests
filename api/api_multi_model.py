 # # -*- coding: utf-8 -*-
# """
# Created on Thu Jul 18 11:02:30 2019
# @author: Yan
# """
# # best resources are https://scotch.io/bar-talk/processing-incoming-request-data-in-flask
# # https://blog.keras.io/building-a-simple-keras-deep-learning-rest-api.html?source=post_page
# # https://towardsdatascience.com/deploying-a-keras-deep-learning-model-as-a-web-application-in-p-fc0f2354a7ff

import tensorflow as tf
from flask import Flask, Response
from flask_restful import Resource, Api, reqparse, request
# from flask_jwt import JWT, jwt_required
from datetime import datetime
from time import perf_counter
import logging, os, json, sys, uuid
import numpy as np

class MultiModelInference:

    logger = None
    loaded_models = []
    identification_styles = [ "original", "batch", "both", "batch_incl_original" ]
    identification_style = "original"
    identification_batch_size = 16
    batch_transformations = {
        "width_shift_range" : [-0.1,-0.1],
        "height_shift_range" : [-0.1,-0.1],
        "rotation_range" : 5,
        "zoom_range" : 0.1
    }
    allowed_extensions = { ".jpg", ".jpeg", ".png", ".gif" }
    model_filename = "model.hdf5"
    classes_filename = "classes.json"

    def __init__(self):
        physical_devices = tf.config.list_physical_devices('GPU')
        try:
            tf.config.experimental.set_memory_growth(physical_devices[0], True)
        except:
            # Invalid device or cannot modify virtual devices once initialized.
            pass

    def initialize_logger(self,logfile_path,log_level=logging.INFO,log_to_screen=False):
        self.logger = logging.getLogger("API")
        self.logger.setLevel(log_level)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

        if not logfile_path==False:
            fh = logging.FileHandler(logfile_path)
            fh.setLevel(log_level)
            fh.setFormatter(formatter)
            self.logger.addHandler(fh)

        if log_to_screen or logfile_path==False:
            ch = logging.StreamHandler()
            ch.setLevel(log_level)
            ch.setFormatter(formatter)
            self.logger.addHandler(ch)

            if logfile_path==False:
                self.logger.warning("no logfile specified; logging only to stdout")

        self.logger.info("TensorFlow v{}".format(tf.__version__))

    def load_model(self,this_model):
        model = os.path.join(this_model["model_path"],self.model_filename)

        with open(os.path.join(this_model["model_path"],self.classes_filename)) as f:
            classes = json.load(f)

        self.loaded_models.append({
            "label": this_model["label"],
            "id": this_model["model_path"].strip('/').split('/')[-1:].pop(),
            "selector": this_model["selector"],
            "model": tf.keras.models.load_model(model,compile=False),
            "classes": classes
        })

        self.logger.info("loaded '{label}' from {model_path}; selector: {selector}".format(
            label=this_model["label"],
            model_path=this_model["model_path"],
            selector=this_model["selector"]))

    def set_identification_style(self,style):
        if style in self.identification_styles:
            self.identification_style = style
        else:
            raise ValueError("unknown identification style: {} ({})".format(self.style,";".join(self.identification_styles)))

    def get_identification_style(self):
        return self.identification_style

    def set_identification_batch_size(self,size):
        self.identification_batch_size = size

    def set_batch_transformations(self,transformations):
        self.batch_transformations = transformations

    def extension_allowed(self,filename):
        filename, file_extension = os.path.splitext(filename)
        return file_extension.lower() in self.allowed_extensions

    def model_exists(self,selector):
        model = [ x for x in self.loaded_models if x["selector"]==selector ]
        return len(model)==1

    def set_model(self,selector):
        if self.model_exists(selector):
            model = [ x for x in self.loaded_models if x["selector"]==selector ]
            self.active_model = model[0]
        else:
            raise ValueError("unknown model: {}".format(selector))

    def get_active_model(self):
        return self.active_model

    def get_loaded_models(self):
        return self.loaded_models

    def preprocess_image(self,raw_image):
        x = tf.keras.preprocessing.image.load_img(
            raw_image,
            target_size=(299,299),
            interpolation="nearest")

        x = tf.keras.preprocessing.image.img_to_array(x)
        x = np.expand_dims(x, axis=0)

        x = x[..., :3]  # remove alpha channel if present
        if x.shape[3] == 1:
            x = np.repeat(x, axis=3, repeats=3)
        x /= 255.0
        # x = (x - 0.5) * 2.0 # why this, laurens?

        return x

    def predict(self,image):
        return self.active_model["model"].predict(image)

    def predict_on_batch(self,batch):
        return self.active_model["model"].predict_on_batch(batch)

    def generate_augmented_image_batch(self,original):
        self.logger.info("identification_batch_size: {}".format(self.identification_batch_size))
        self.logger.info("batch_transformations: {}".format(self.batch_transformations))

        b = self.batch_transformations

        datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            width_shift_range=b["width_shift_range"] if "width_shift_range" in b else 0.0,
            height_shift_range=b["height_shift_range"] if "height_shift_range" in b else 0.0,
            rotation_range=b["rotation_range"] if "rotation_range" in b else 0,
            zoom_range=b["zoom_range"] if "zoom_range" in b else 0.0
        )

        batch = []

        if self.identification_style == "batch_incl_original":
            batch.append(original[0])

        it = datagen.flow(original, batch_size=1)

        for i in range(self.identification_batch_size-len(batch)):
            next_batch = it.next()
            image = next_batch[0]
            batch.append(image)

        return np.array(batch)

mmi = MultiModelInference()

app = Flask(__name__)
# app.config['SECRET_KEY'] = os.getenv('JWT_KEY')
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['UPLOAD_FOLDER'] = '/tmp'

# @jwt_required()
@app.route("/<selector>/identify",methods=["POST"])
def identify_image(selector):
    global mmi

    if not mmi.model_exists(selector):
        mmi.logger.info("unknown model: {}".format(selector))
        return { "error" : "unknown model: {}".format(selector) }
    else:
        mmi.logger.info("using model: {}".format(selector))

    uploaded_files = request.files.getlist("image")

    if len(uploaded_files)<1:
        return { "error" : "no file" }
    else:
        file = uploaded_files[0]

    mmi.logger.info("file: {}".format(file))

    if file and mmi.extension_allowed(file.filename):

        unique_filename = os.path.join(app.config['UPLOAD_FOLDER'], str(uuid.uuid4()))
        file.save(unique_filename)

        prediction_start = perf_counter()

        mmi.set_model(selector)
        mmi.logger.info("identification_style: {}".format(mmi.get_identification_style()))

        x = mmi.preprocess_image(unique_filename)

        predictions_batch = None

        if mmi.get_identification_style() in [ "original", "both" ]:
            predictions_original = mmi.predict(x)
            predictions_original = predictions_original[0].tolist()

        if mmi.get_identification_style() in [ "batch", "both", "batch_incl_original" ]:
            batch = mmi.generate_augmented_image_batch(x)
            predictions_batch = mmi.predict_on_batch(batch)
            if identification_style == "batch_incl_original":
                predictions_original = predictions_batch[0].tolist()
            predictions_batch = np.mean(predictions_batch,axis=0)
            predictions_batch = predictions_batch.tolist()

        os.remove(unique_filename)

        classes = {k: v for k, v in sorted(mmi.get_active_model()["classes"].items(), key=lambda item: item[1])}

        results_original = None
        results_batch = None

        if not predictions_original is None:
            predictions_original = dict(zip(classes.keys(), predictions_original))
            predictions_original = {k: v for k, v in sorted(predictions_original.items(), key=lambda item: item[1], reverse=True)}

            results_original = []
            for key in predictions_original:
                results_original.append({ 'class' : key, 'prediction': predictions_original[key] })

        if not predictions_batch is None:
            predictions_batch = dict(zip(classes.keys(), predictions_batch))
            predictions_batch = {k: v for k, v in sorted(predictions_batch.items(), key=lambda item: item[1], reverse=True)}

            results_batch = []
            for key in predictions_batch:
                results_batch.append({ 'class' : key, 'prediction': predictions_batch[key] })

        if not results_batch is None:
            mmi.logger.info("prediction (batch): {}".format(results_batch[0]))

        if not results_original is None:
            mmi.logger.info("prediction (original): {}".format(results_original[0]))

        mmi.logger.info("time taken: {}".format(perf_counter()-prediction_start))

        if not results_batch is None:
            output = { 'predictions' : results_batch }
            if not results_original is None:
                output['predictions_original'] = results_original
        else:
            output = { 'predictions' : results_original }

        output['model'] = { 'label' : mmi.get_active_model()['label'], 'id' : mmi.get_active_model()['id'] }
        output['timestamp'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f%z')

        response = output

    else:
        response = { "error" : "unsupported file type" }

    return Response(json.dumps(response), mimetype='application/json')

@app.route("/",methods=["GET","POST"])
def root():
    return { "naturalis museumproject inference api" : "v0.1 (multi model version)" }

@app.route("/models",methods=["GET","POST"])
def models():
    global mmi
    m = []
    for model in mmi.get_loaded_models():
        m.append({
            "label" : model["label"],
            "id" : model["id"],
            "path" : "/{}/identify".format(model["selector"]),
            "classes" : len(model["classes"])
        })

    return Response(json.dumps({ "models" : m }), mimetype='application/json')

@app.errorhandler(404)
def page_not_found(e):
    return Response(json.dumps({ "error" : e.description }), mimetype='application/json', status=404)

mmi.initialize_logger(
    os.getenv('API_LOGFILE_PATH',False),
    logging.DEBUG if os.getenv('API_DEBUG')=="1" else logging.INFO,
    os.getenv('API_DEBUG')=="1"
)

modelstring = os.getenv('API_MODELS',False)

if modelstring==False:
    raise ValueError("no models to load (missing API_MODELS)")

for model in [x for x in filter(lambda x: len(x)>0,json.loads(modelstring))]:
    mmi.load_model(model)

if not os.getenv('API_IDENTIFICATION_STYLE') is None:
    mmi.set_identification_style(os.getenv('API_IDENTIFICATION_STYLE'))

if not os.getenv('API_BATCH_IDENTIFICATION_SIZE') is None:
    mmi.set_identification_batch_size(int(os.getenv('API_BATCH_IDENTIFICATION_SIZE')))

if not os.getenv('API_BATCH_TRANSFORMATIONS') is None:
    mmi.set_batch_transformations(json.loads(os.getenv('API_BATCH_TRANSFORMATIONS')))


if __name__ == '__main__':
    app.run(debug=(os.getenv('API_FLASK_DEBUG')=="1"),host='0.0.0.0')
else:
     print('\n * server ready')


# TODO: tokens, users

# sudo docker-compose run api /api/api_multi_model.py (have we tried compose yet?)
# with gunicorn:
# sudo docker-compose run api
# curl -s -XPOST  -F "image=@spitsmuis.jpg" 192.168.96.2:5000/sharks/identify

# # .env
# API_LOGFILE_PATH=/log/general.log
# API_DEBUG=1
# API_IDENTIFICATION_STYLE=batch_incl_original
# API_BATCH_IDENTIFICATION_SIZE=8
# API_BATCH_TRANSFORMATIONS={"width_shift_range": [-0.2, -0.2], "height_shift_range": [-0.2, -0.2], "rotation_range": 0.1, "zoom_range": 0 }
# API_MODELS=[{"label" : "vlinders", "model_path" : "/data/museum/naturalis/models/20211208-090838/", "selector" : "papillotten" },{"label" : "haaien", "model_path" : "/data/museum/shark_eggs/models/20211209-111446/", "selector" : "sharks" },{"label" : "conidae", "model_path" : "/data/museum/conidae/models/20220112-132211/", "selector" : "conidae" }]
# API_FLASK_DEBUG=0 (avoid)

# API_IDENTIFICATION_STYLE
# "original"  --> predict on original image only (default)
# "batch"     --> batch predict on API_BATCH_IDENTIFICATION_SIZE (default 16) generated images, using API_BATCH_TRANSFORMATIONS, and takes the mean
# "both"      --> both of the above (besides the standard 'predictions', output has 'predictions_original')
# "batch_incl_original"
#             --> batch, but with the first augmented image of the batch replaced with the original
