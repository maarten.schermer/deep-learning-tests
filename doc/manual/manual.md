# Deep Learning for image recognition

-----



## Training
#BASE_MODEL=MobileNetV2
#BASE_MODEL=ResNet50
#BASE_MODEL=VGG16
#BASE_MODEL=InceptionResNetV2
BASE_MODEL=InceptionV3
#BASE_MODEL=Xception

also default:
BASE_MODEL=InceptionV3
the others havent' been tested for a long time!


LEARNING_RATE=[ 0.0001 ]
BATCH_SIZE=32
CHECKPOINT_MONITOR=val_acc




defaults:

        self.presets.update( { "base_model" : "InceptionV3" } )
        self.presets.update( { "batch_size" : 64 } )
        self.presets.update( { "checkpoint_monitor" : "val_acc" } )
        self.presets.update( { "class_image_maximum" : 0 } )
        self.presets.update( { "class_image_minimum" : 2 } )
        self.presets.update( { "downsampling_ratio" : -1 } )
        self.presets.update( { "early_stopping_monitor" : [ "none" ] } )
        self.presets.update( { "early_stopping_patience" : [ 10 ] } )
        self.presets.update( { "epochs" : [ 200 ] } )
        self.presets.update( { "freeze_layers" : [ "none" ] } )
        self.presets.update( { "image_augmentation" : None } )
        self.presets.update( { "image_list_class_column" : 0 } )
        self.presets.update( { "image_list_file_column" : 2 } )
          col 1 is the original URL
        self.presets.update( { "learning_rate" : [ 1e-4 ] } )
        self.presets.update( { "metrics" : [ "acc" ] } )
        self.presets.update( { "reduce_lr_params" : [ {"monitor": "val_loss", "factor": 0.1, "patience": 4, "min_lr": 1e-09, "verbose": 1}] } )
        self.presets.update( { "upsampling_ratio" : -1 } )
        self.presets.update( { "use_class_weights" : False } )
        self.presets.update( { "use_imagenet_weights" : True } )
        self.presets.update( { "use_tensorboard" : True } )
        self.presets.update( { "validation_split" : 0.2 } )



If, for some reason, you don't want your training images to reside in the 'images' folder, you can override the location in the .env file:
```bash
IMAGES_ROOT=/data/ai/projects/my_cool_project/better_images/
```
