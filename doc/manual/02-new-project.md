# Deep Learning pipeline for image recognition
## Project definition

[Back to index](00-index.md)

### Create .env file
All relevant parameters are in an .env file, which should be located in the 'docker' folder of the cloned repo (there's a 'template.env' there, which might not be totally up to date). Add the following, which is the bare minimum (we'll be adding more later):

```bash
PROJECT_NAME=my cool project
PROJECT_ROOT=/data/projects/my_cool_project/
```
**Take note**: the PROJECT_ROOT is accessed by the application from *within* the 'tensorflow' docker container. This means that the path should be defined as it is "seen" by the container. Check the 'volumes' specification of the 'tensorflow'-section in the docker-compose.yml file to see the mapping. By default (at the time of writing) it looks like this:
```yml
  [...]
  tensorflow:
  [...]
    volumes:
      - /data/maarten.schermer/data:/data
      - /data/maarten.schermer/deep-learning-tests/code:/code
  [...]

```
So, in this example, '/data/projects/my_cool_project/' would translate to '/data/maarten.schermer/data/projects/my_cool_project/' on the host server.

### Preparing your project
Next, go into the 'docker' folder and run the following command:

```bash
sudo docker-compose run tensorflow /code/project_prepare.py
```

This will create some folders in your project root:
+ dwca (for DwCA-files, if any)
+ images (root folder for training images)
+ lists (folder for initial data files)
+ log
+ models (where all your trained models will go)
+ results (empty folder which you can use to write any inference results to)

Take note: the folder specified in PROJECT_ROOT must already exist for this to work.