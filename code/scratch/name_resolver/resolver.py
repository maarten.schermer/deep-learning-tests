import argparse, csv, json, re
import urllib.parse, urllib.request

'''
This script aims to resolve scientific names to accepted scientific names from the Catalogue of Life (CoL) backbone.

The script takes to parameters:

--input_file (mandatory)
--output_file (optional; defaults to ./output.tsv)

The input should be a text file with a list of scientifc names, one per line (lines that start with # are ignored).

The script:

1. Pre-processes names: '(f)' and '(m)' are removed from the string, as well as other possible artifacts.
2. Looks up the cleaned up names through the GBIF's name match API, resulting in a list of possible matches.
   Responsed typically have one main match, and optionally an array of alternative matches. Each match comes
   with a confidence score.
3. From all matches, the one with the highest confidence is selected, providing that
   - Its rank is in self.acceptable_ranks (skipped ranks are printed to the screen).
   - Its confidence score is higher than self.minimum_confidence_score.
4. If the match is a synonym, the accepted name is retrieved uing the species lookup API, using the speciesKey that comes with the match.
5. The results are then written to the output file. Output has six colums:
   - raw_input: input, as is
   - lookup: input, cleaned
   - resolved_name: best match scientific name
   - resolved_canonical: best match nomen
   - resolved_status: best match status
   - resolved_rank: best match rank
   - match_confidence: best match confidence
   - resolved_matches_input: true if input is identical to match (full name or nomen)
   - class: suggested class to use for training
'''

class NameResolver():

    input_file = None
    output_file = "./output.tsv"
    names = []

    minimum_confidence_score = 75
    acceptable_ranks = [ "SPECIES", "SUBSPECIES", "VARIETY", "FORM" ]

    # url_col_name_parser = 'https://api.catalogueoflife.org/parser/name?name={name}'
    url_gbif_name_match = 'https://api.gbif.org/v1/species/match?verbose=true&name={name}'
    url_gbif_species_lookup = 'https://api.gbif.org/v1/species/{key}'

    def info(self,msg):
        print(msg)

    def set_input_file(self,input_file):
        self.input_file = input_file
        self.info("input: {}".format(self.input_file))

    def set_output_file(self,output_file):
        self.output_file = output_file
        self.info("output: {}".format(self.output_file))

    def set_minimum_confidence_score(self,minimum_confidence_score):
        self.minimum_confidence_score = minimum_confidence_score
        self.info("minimum_confidence_score: {}".format(self.minimum_confidence_score))

    def set_acceptable_ranks(self,acceptable_ranks):
        self.acceptable_ranks = acceptable_ranks

    def read_input_file(self):
        with open(self.input_file, 'r', encoding='utf-8-sig') as file:
            reader = csv.reader(file,delimiter='\t')
            names = [x[0] for x in list(reader)]

        for name in names:
            if name[0:1]!="#":
                self.names.append({"raw" : name.strip() })

        self.info("read {} names".format(len(self.names)))

    def preprocess_names(self):
        for name in self.names:
            self.current_name = name
            self.preprocess_name()

    def preprocess_name(self):
        p = [x for x in self.names if x["raw"]==self.current_name["raw"] and "lookup" in x ]

        if len(p)>0:
            self.current_name["lookup"] = p[0]["lookup"]
        else:
            replacements = [
                {"element" : "(f)", "replacement" : "" },
                {"element" : "(m)", "replacement" : "" },
                {"element" : "_", "replacement" : " " }
            ]

            self.current_name["lookup"] = self.current_name["raw"]

            for replacement in replacements:
                self.current_name["lookup"] = self.current_name["lookup"].replace(replacement["element"],replacement["replacement"])

            self.current_name["lookup"] = re.sub(r"\s+", " ", self.current_name["lookup"])

    def gbif_lookup_names(self):
        i=0
        for name in self.names:
            self.current_name = name
            self.gbif_lookup_name()
            print("{}  {} --> {} [{}]".format(
                "v" if (
                    "best_match" in self.current_name and
                    self.current_name["best_match"]["status"]!="NONE" and
                    (
                        self.current_name["raw"]==self.current_name["best_match"]["resolved_name"] or
                        self.current_name["raw"]==self.current_name["best_match"]["resolved_nomen"]
                    )
                )
                else ".",
                self.current_name["raw"],
                self.current_name["best_match"]["resolved_name"]
                    if ("best_match" in self.current_name and self.current_name["best_match"]["status"]!="NONE") else "-",
                self.current_name["best_match"]["confidence"]
                    if ("best_match" in self.current_name and "confidence" in self.current_name["best_match"]) else "-"
                )
            )

            i+=1
            if i % 100 == 0:
                self.info("looked up {} name usages".format(i))

    # def get_similarity(self,note):
    #     return {x[0]: int(x[1]) if 1 in x else -1 for x in list(map(lambda x: x.lstrip().split('='),note.replace("Similarity:","").split(";")))}

    def get_values(self,data):
        return {
            "canonicalName": data["canonicalName"],
            "confidence": data["confidence"],
            "matchType": data["matchType"],
            "note": data["note"],
            # "similarity": self.get_similarity(data["note"]),
            "rank": data["rank"],
            "scientificName": data["scientificName"],
            "status": data["status"],
            "synonym": data["synonym"],
            "species": data["species"] if "species" in data else None,
            "speciesKey": data["speciesKey"] if "speciesKey" in data else None,
        }

    def gbif_lookup_name(self):
        p = [x for x in self.names if x["raw"]==self.current_name["raw"] and "best_match" in x ]

        if len(p)>0:

            self.current_name["best_match"] = p[0]["best_match"]

        else:

            best_match = { "confidence" : 0 }

            if "lookup" in self.current_name:

                url = self.url_gbif_name_match.format(name=urllib.parse.quote(self.current_name["lookup"]))

                response = urllib.request.urlopen(url)
                data = response.read()
                data = json.loads(data)

                # self.info(json.dumps(data, indent=4, sort_keys=True));

                best_confidence = 0
                best_match = {}

                if data["matchType"]=="NONE":
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.info("{}: no match".format(self.current_name["raw"]))
                    return


                if data["rank"] in self.acceptable_ranks:
                    if data["confidence"] > best_confidence:
                        best_match = self.get_values(data)
                        best_confidence = data["confidence"]

                else:
                    self.info("skipping {}".format(data["rank"]))


                if "alternatives" in data:
                    for alternative in data["alternatives"]:
                        if alternative["rank"] in self.acceptable_ranks:
                            if alternative["confidence"] > best_confidence and alternative["rank"] in self.acceptable_ranks:
                                best_match = self.get_values(alternative)
                                best_confidence = alternative["confidence"]
                        else:
                            self.info("skipping {}".format(data["rank"]))

                # self.info(best_match)

                if not "scientificName" in best_match and not "canonicalName" in best_match:
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.info("{}: no match".format(self.current_name["raw"]))
                    return


                if best_confidence < self.minimum_confidence_score:
                    self.current_name["best_match"] = { "status" : "NONE" }
                    self.info("{}: best confidence too low ({})".format(self.current_name["raw"],best_confidence))
                    return

                best_match["resolved_name"] = best_match["scientificName"]
                best_match["resolved_nomen"] = best_match["canonicalName"]

                if best_match["status"]=="SYNONYM":
                    if "speciesKey" in best_match:

                        url = self.url_gbif_species_lookup.format(key=urllib.parse.quote(str(best_match["speciesKey"])))

                        response = urllib.request.urlopen(url)
                        data = response.read()
                        data = json.loads(data)

                        # self.info(json.dumps(data, indent=4, sort_keys=True));

                        best_match["resolved_name"] = data["scientificName"]
                        best_match["resolved_nomen"] = data["canonicalName"]
                    elif "species" in best_match:
                        best_match["resolved_name"] = best_match["species"]

                elif best_match["status"]!="ACCEPTED":
                    self.info("weird status: {}".format(best_match["status"]))

            else:
                self.info("{}: no nomen!?".format(self.current_name["raw"]))

            self.current_name["best_match"] = best_match

    def write_names(self):

        header = [
            "raw_input",
            "lookup",
            "resolved_name",
            "resolved_canonical",
            "resolved_status",
            "resolved_rank",
            "match_confidence",
            "resolved_matches_input",
            "class"
        ]

        with open(self.output_file, 'w', newline='') as csvfile:

            delimiter='\t'
            writer = csv.writer(csvfile, delimiter=delimiter,quoting=csv.QUOTE_MINIMAL)
            writer.writerow(header)

            for name in self.names:
                bm = name["best_match"] if "best_match" in name else {}
                row = [
                    name["raw"],
                    name["lookup"],
                    bm["resolved_name"] if "resolved_name" in bm else "",
                    bm["resolved_nomen"] if "resolved_nomen" in bm else "",
                    bm["status"] if "status" in bm else "",
                    bm["rank"] if "rank" in bm else "",
                    bm["confidence"] if "confidence" in bm else "",
                    (bm["resolved_name"]==name["raw"] or bm["resolved_nomen"]==name["raw"]) if "resolved_name" in bm else False,
                    bm["resolved_name"] if "resolved_name" in bm else name["raw"],
                ]
                writer.writerow(row)

        self.info(self.output_file)


    # def col_parse_names(self):
    #     i=0
    #     for name in self.names:
    #         self.current_name = name
    #         self.col_parse_name()
    #         i+=1
    #         if i % 100 == 0:
    #             self.info("parsed {} names".format(i))

    #     self.info("parsed {} names".format(i))

    # def col_parse_name(self):
    #     p = [x for x in self.names if x["raw"]==self.current_name["raw"] and "parsed_name" in x ]

    #     if len(p)>0:
    #         self.current_name["parsed_name"] = p[0]["parsed_name"]
    #     else:
    #         url = self.url_col_name_parser.format(name=urllib.parse.quote(self.current_name["lookup"]))
    #         response = urllib.request.urlopen(url)
    #         data = json.loads(response.read())
    #         self.current_name["parsed_name"] = data

    # def parsed_name_set_nomen(self):
    #     if self.current_name["parsed_name"]["rank"]=="species":
    #         self.current_name["nomen"] = "{} {}".format(
    #             self.current_name["parsed_name"]["genus"],
    #             self.current_name["parsed_name"]["specificEpithet"]
    #         )
    #     elif self.current_name["parsed_name"]["rank"]=="infraspecific name":
    #         self.current_name["nomen"] = "{} {} {}".format(
    #             self.current_name["parsed_name"]["genus"],
    #             self.current_name["parsed_name"]["specificEpithet"],
    #             self.current_name["parsed_name"]["infraspecificEpithet"]
    #         )
    #     else:
    #         self.info("{}: unexpected rank '{}'".format(self.current_name["raw"],self.current_name["parsed_name"]["rank"]))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file",type=str)
    parser.add_argument("--output_file",type=str)
    parser.add_argument("--minimum_confidence_score",type=int,default=75)
    args = parser.parse_args()

    n = NameResolver()

    if args.input_file:
        n.set_input_file(args.input_file)
    else:
        raise ValueError("need an input file (--input_file <filename>)")

    if args.output_file:
        n.set_output_file(args.output_file)

    n.set_minimum_confidence_score(args.minimum_confidence_score)
    n.set_acceptable_ranks([ "SPECIES", "SUBSPECIES", "FORM" ])

    n.read_input_file()
    n.preprocess_names()
    n.gbif_lookup_names()
    n.write_names()

