import os, csv, argparse
from lib import baseclass, utils

class ClassListCreator(baseclass.BaseClass):

    classes = {}
    override_image_list = None
    override_class_list = None

    def __init__(self):
        super().__init__()

    def set_override_image_list(self,override_image_list):
        self.override_image_list = override_image_list
        self.logger.info("set alternative image list: {}".format(self.override_image_list))

    def set_override_class_list(self,override_class_list):
        self.override_class_list = override_class_list
        self.logger.info("set alternative class list: {}".format(self.override_class_list))

    def get_classes(self):
        self.image_list_class_col = clc.get_preset("image_list_class_column")
        # self.image_list_image_col = clc.get_preset("image_list_file_column")

        if self.override_image_list:
            downloaded_images_file = self.override_image_list
        else:
            downloaded_images_file = self.downloaded_images_file

        delimiter = utils._determine_csv_delimiter(downloaded_images_file,encoding="utf-8-sig")

        with open(downloaded_images_file) as csv_file:
            # dialect = csv.Sniffer().sniff(csv_file.read(1024), delimiters=";,\t")
            # csv_file.seek(0)
            # csv_reader = csv.reader(csv_file, dialect)
            csv_reader = csv.reader(csv_file,delimiter=delimiter)

            for row in csv_reader:
                this_class = row[self.image_list_class_col]
                if this_class in self.classes:
                    self.classes[this_class] += 1
                else:
                    self.classes[this_class] = 1

        self.logger.info("found {} classes".format(len(self.classes)))

    def write_classes(self):
        if self.override_class_list:
            class_file = self.override_class_list
        else:
            class_file = self.get_class_list_path()

        with open(class_file,'w') as csvfile:
            s = csv.writer(csvfile)
            for this_class in self.classes:
                s.writerow([this_class,self.classes[this_class]])

        self.logger.info("wrote class list: {}".format(class_file))


if __name__ == "__main__":

    clc = ClassListCreator()
    clc.set_project(os.environ)
    clc.set_presets(os_environ=os.environ)

    parser = argparse.ArgumentParser()
    parser.add_argument("--override_image_list",type=str)
    parser.add_argument("--override_class_list",type=str)
    args = parser.parse_args()

    if args.override_image_list:
        clc.set_override_image_list(args.override_image_list)

    if args.override_class_list:
        clc.set_override_class_list(args.override_class_list)

    clc.get_classes()
    clc.write_classes()


