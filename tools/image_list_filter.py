import argparse, csv, json, re, os
from lib import logclass

class ImageListFilter():

    image_list_file = None
    class_filter_file = None
    output_file = None
    output_file_skipped = None
    csv_delimiter = None
    image_list_lines = []
    class_filter_lines = []
    matched_lines = []
    skipped_lines = []
    class_col = 0

    def __init__(self):
        pr = os.environ.get("PROJECT_ROOT")

        if pr is None:
            raise ValueError("need a project root (PROJECT_ROOT missing from .env)")

        logfile = os.path.join(pr, "log", "general.log")

        if not os.path.exists(logfile):
            path = os.path.join(pr, "log")
            if not os.path.exists(path):
                os.mkdir(path)
                os.chmod(path,0o777)

            with open(logfile, 'w') as fp:
                pass

            os.chmod(logfile,0o777)

        self.logger = logclass.LogClass(self.__class__.__name__,logfile)

    def set_image_list_file(self,image_list_file):
        self.image_list_file = image_list_file
        self.logger.info("image_list_file: {}".format(self.image_list_file))

    def set_class_filter_file(self,class_filter_file):
        self.class_filter_file = class_filter_file
        self.logger.info("class_filter_file: {}".format(self.class_filter_file))

    def set_output_file(self,output_file):
        self.output_file = output_file
        self.logger.info("output: {}".format(self.output_file))

    def set_output_file_skipped(self,output_file_skipped):
        self.output_file_skipped = output_file_skipped
        self.logger.info("output_file_skipped: {}".format(self.output_file_skipped))

    def set_csv_delimiter(self,csv_delimiter):
        self.csv_delimiter = "\t" if csv_delimiter =='[tab]' else csv_delimiter
        self.logger.info("csv_delimiter: {}".format(self.csv_delimiter))

    def read_image_list_file(self):
        with open(self.image_list_file, 'r', encoding='utf-8-sig') as csv_file:
            reader = csv.reader(csv_file, delimiter=self.csv_delimiter)
            self.image_list_lines = list(reader)

        self.logger.info("read {} lines".format(len(self.image_list_lines)))

    def read_class_filter_file(self):
        class_filter_lines = []
        with open(self.class_filter_file, 'r', encoding='utf-8-sig') as csv_file:
            reader = csv.reader(csv_file, delimiter=self.csv_delimiter)
            for row in reader:
                class_filter_lines.append(row[0])

        self.class_filter_lines = list(set(class_filter_lines))
        self.logger.info("read {} unique classes (from {} total)".format(len(self.class_filter_lines),len(class_filter_lines)))

    def do_filter(self):
        for line in self.image_list_lines:
            a = [x for x in self.class_filter_lines if x==line[self.class_col].strip()]
            if len(a)>0:
                self.matched_lines.append(line)
            else:
                self.skipped_lines.append(line)

        self.logger.info("retined {} lines, skipped {}".format(len(self.matched_lines),len(self.skipped_lines)))

    def write_output(self):
        with open(self.output_file, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=self.csv_delimiter,quoting=csv.QUOTE_MINIMAL)
            for line in self.matched_lines:
                writer.writerow(line)

        self.logger.info("wrote output: {}".format(self.output_file))

    def write_skipped_output(self):
        with open(self.output_file_skipped, 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=self.csv_delimiter,quoting=csv.QUOTE_MINIMAL)
            for line in self.skipped_lines:
                writer.writerow(line)

        self.logger.info("wrote skipped lines: {}".format(self.output_file_skipped))



if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--image_list_file",type=str,required=True)
    parser.add_argument("--class_filter_file",type=str,required=True)
    parser.add_argument("--output_file",type=str,required=True)
    parser.add_argument("--output_file_skipped",type=str,required=True)
    parser.add_argument("--csv_delimiter", type=str, default=",")
    args = parser.parse_args()

    n = ImageListFilter()
    n.set_image_list_file(args.image_list_file)
    n.set_class_filter_file(args.class_filter_file)
    n.set_output_file(args.output_file)
    n.set_output_file_skipped(args.output_file_skipped)
    n.set_csv_delimiter(args.csv_delimiter)
    n.read_image_list_file()
    n.read_class_filter_file()
    n.do_filter()
    n.write_output()
    n.write_skipped_output()

