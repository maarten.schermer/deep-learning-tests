import os, argparse, glob
from PIL import Image
# from lib import baseclass, utils, dataset

class ImageVerifier:

    root_dir = None
    bad_files = []
    image_list = []
    current_image_file = None
    image_list_file = None
    filepath_col = None
    override_image_root_folder = None
    prepend_image_root_folder = None
    progress = 0
    set_check_bands_only = False

    def set_root_dir(self,root_dir):
        self.root_dir = root_dir

    def set_check_bands_only(self,status):
        self.set_check_bands_only = status

    def set_current_image_file(self,current_image_file):
        self.current_image_file = current_image_file

    def set_image_list_file(self,image_list_file):
        self.image_list_file = image_list_file

    def set_image_list_filepath_column(self,filepath_col):
        self.filepath_col = filepath_col

    def set_override_image_root_folder(self,folder):
        self.override_image_root_folder = folder

    def set_prepend_image_root_folder(self,folder):
        self.prepend_image_root_folder = folder

    def get_bad_files(self):
        return self.bad_files

    def verify_image(self):
        # img = Image.open(filename) # open the image file
        # img.verify() # verify that it is, in fact an image
        im = Image.open(self.current_image_file)
        im.verify() #I perform also verify, don't know if he sees other types o defects
        # im.close() #reload is necessary in my case
        im = Image.open(self.current_image_file)
        im.transpose(Image.FLIP_LEFT_RIGHT)
        # im.close()

    def get_bands(self):
        im = Image.open(self.current_image_file)
        im1 = Image.Image.getbands(im)
        if (im1!=('R','G','B')):
            raise ValueError("non-three band image: {}".format(im1))

    def verify_images_from_folder(self):
        self.progress = 0
        for extension in [".jpg",".jpeg",".png"]:
            path = self.root_dir + '/**/*' + extension
            print(path)
            for filename in glob.iglob(path, recursive=True):
                if os.path.isfile(filename):
                    try:
                        self.set_current_image_file(filename)
                        if not self.set_check_bands_only:
                            self.verify_image()
                        self.get_bands()
                    except Exception as e:
                        self.bad_files.append({ "filename": filename, "error": str(e)})
                        print("filename: {}; error: {}".format(filename,str(e)))
                    self.progress += 1
                    self._print_progress()

        print("done")

    def verify_images_from_image_list(self):
        self.progress = 0
        self.image_list = []
        self.read_image_list_file()
        for filename in self.image_list:
            try:
                self.set_current_image_file(filename)
                self.verify_image()
            except Exception as e:
                self.bad_files.append({ "filename": filename, "error": str(e)})
            self.progress += 1
            self._print_progress()

    def _print_progress(self):
        if self.progress % 500 == 0:
            print("{} / {} ".format(len(self.bad_files),self.progress))

    def read_image_list_file(self):
        with open(self.image_list_file) as csv_file:
            # reader = csv.reader(csv_file, delimiter=utils._determine_csv_separator(self.downloaded_images_file,"utf-8-sig"))
            reader = csv.reader(csv_file, delimiter=_determine_csv_delimiter(self.image_list_file,"utf-8-sig"))
            for row in reader:
                file = row[self.filepath_col]

                if self.override_image_root_folder:
                    file = os.path.join(self.override_image_root_folder, os.path.basename(file))

                if self.prepend_image_root_folder:
                    file = os.path.join(self.prepend_image_root_folder, file)

                if row[0] and file:
                    self.image_list.append(file)

        print("read image list {}, found {} entries".format(self.image_list_file,len(self.image_list)))

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--image_root",type=str, required=True)
    parser.add_argument("--check_bands_only",action="store_true")
    args = parser.parse_args()

    img = ImageVerifier()
    print("checking for corrupt files in {}".format(args.image_root))
    img.set_root_dir(args.image_root)
    img.set_check_bands_only(args.check_bands_only)

    img.verify_images_from_folder()
    bad_files = img.get_bad_files()

    for file in bad_files:
        print("found corrupt file: {} ({})".format(file["filename"],file["error"]))